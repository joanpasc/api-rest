# Backend CRUD API REST

_Ejemplo de WS REST con NodeJS que proporciona un API CRUD para gestionar una DB MongoDB._

## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._

Ver **Deployment** para conocer cómo desplegar el proyecto.


### Pre-requisitos 📋

_Necesitamos instalar npm, VisualStudio Code, git, postman, Mongodb, node, Express_



```
sudo snap install --classic code
sudo snap install postman
sudo apt install npm
sudo apt install git
sudo npm i -g n
sudo npm i -S express
sudo apt install mongodb
sudo apt install mongojs
```

### Instalación 🔧

_Una serie de ejemplos paso a paso que te dice lo que debes ejecutar para tener un entorno de desarrollo ejecutandose_

_Se tendrá que instalar todos los pre-requisitos nombrados anteriormente y otros relacionados con éstos para que funcionen correctamente_
_Empezamos instalando visualStudio Code, postman y npm_

```
sudo snap install --classic code
sudo snap install postman
sudo apt update
sudo apt install npm
```

_Cuando hemos completado las anteriores instalaciones, seguimos instalando node_

```
sudo npm clean -f 
sudo npm i -g n 
sudo n stable 
```
_Ahora instalamos git y creamos o clonamos un repositorio para poder trabajar en otras máquinas, si lo hemos creado, o para empezar a trabajar en esta máquina si hemos hecho un clone_

```
sudo apt install git
git config --global user.name nombreUsuario
git config --global user.email email@usuario.com
```

_Después de haber creado o clonado el repositorio, empezamos a untilizar npm e instalamos express y morgan_


```
npm init
npm i -S express
npm i -S morgan
```
 
_Y ahora instalamos MongoDB, ejecutamos mongodb e instalamos mongojs para trabajar con node_

```
sudo apt install -y mongodb
sudo systemctl start mongo
npm i -S mongodb
npm i -S mongojs
```


_Futuras demos aquí_

## Ejecutando las pruebas ⚙️

_ejecutamos npm start en la terminal para que funcione nuertro servicio api-rest_
_Desde otra terminal ejecutamos mongoDB con:_
```
sudo systemctl start mongo
```
_Y ya podríamos utilizar nuestro servicio api-rest desde cualquier navegador aunque, es mejor utilizar postman para poder hacer más fácil y mejor las inserciones y modificaciones_

### Analice las pruebas end-to-end 🔩

_Explica qué verifican estas pruebas y por qué_

```
Proporciona un ejemplo
```

### Y las pruebas de estilo de codificación ⌨️

_Explica qué verifican estas pruebas y por qué_

```
Proporciona un ejemplo
```

## Despliegue 📦

_Agrega notas adicionales sobre cómo hacer deploy_

## Construido con 🛠️

_Menciona las herramientas que utilizaste para crear tu proyecto_

* [Bitbucket](https://bitbucket.org/) - El repositorio web utilizado
* [Postman](https://www.postman.com/) - Utilizado para realizar las pruebas del funcionamiento de la práctica
* [VisualStudio Code](https://code.visualstudio.com/) - utiliado para programar el proyecto

## Contribuyendo 🖇️

Por favor lee el [CONTRIBUTING.md](https://gist.github.com/tu/tuProyecto) para detalles de nuestro código de conducta, y el proceso para enviarnos pull requests.

## Wiki 📖

Puedes encontrar mucho más de cómo utilizar este proyecto en nuestra [Wiki](https://github.com/tu/proyecto/wiki)

## Versionado 📌

Usamos [SemVer](http://semver.org/) para el versionado. Para todas las versiones disponibles, mira los [tags en este repositorio](https://github.com/tu/proyecto/tags).

## Autores ✒️

_Menciona a todos aquellos que ayudaron a levantar el proyecto desde sus inicios_

* **Joan Pascual** - *El total del proyecto* - [joanPasc](https://bitbucket.org/joanpasc)
* **Paco Maciá** - *Trabajo Inicial* - [pmacia](https://github.com/pmacia)

También puedes mirar la lista de todos los [contribuyentes](https://github.com/your/project/contributors) quiénes han participado en este proyecto. 

## Licencia 📄

Este proyecto está bajo la Licencia (Tu Licencia) - mira el archivo [LICENSE.md](LICENSE.md) para detalles

## Expresiones de Gratitud 🎁

* Comenta a otros sobre este proyecto 📢
* Invita una cerveza 🍺 o un café ☕ a alguien del equipo. 
* Da las gracias públicamente 🤓.
* etc.