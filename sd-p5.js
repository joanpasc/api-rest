'use strict'

const express = require('express'); 
const logger = require('morgan');
const mongojs = require('mongojs'); 
const fs=require('fs');

const PassService = require('./services/pass.service');
const TokenService = require('./services/token.service');
const moment= require('moment');

const app = express();
const cors= require('cors'); 

const https = require('https');
const OPTIONS_HTTPS ={
    key: fs.readFileSync('./cert/key.pem'),
    cert: fs.readFileSync('./cert/cert.pem')
};



var db = mongojs("SD");
var id = mongojs.ObjectID;

var helmet = require('helmet');
const { resolveSoa } = require('dns');
const req = require('express/lib/request');
const res = require('express/lib/response');
//Middleware
app.use(helmet());

var allowCrossTokenHeader = (req, res, next) => {
    res.header("Access-Control-Allow-Headers", "*");
    return next();
};

var allowMethods = (req, res, next) => {
    res.header("Access-Control-Allow-Hethods", "*");
};

var auth = (req, res, next) =>{
    if(req.headers.token === "password1234"){
        return next();
    }else{
        return next(new Error("No autorizado"));
    };
};

var auth = (req, res, next) => {
    const token =req.header.authorization.split(' ')[1];
    TokenService.decodificaToken(token).then( userId => { 
        id=userId,
        token=token
    }).catch(err =>
        res.status(400).json({
            result: 'ko',
            msg: err
        })
    );
}

// Declaramos los middleware 
app.use(logger('dev')); // probar con: tiny, short, dev, common, combined
app.use(express.urlencoded({extended:false}));
app.use(express.json());

//_______________auth______________________________

app.post('/api/auth',(req, res, next) => { 
    const elemento = req.body; 
    console.log(elemento);
  
    if (!elemento.email) { 
      res.status(400).json ({ 
      error: 'Bad data', 
      description: 'Se precisa al menos un campo <emial>' 
      }); 
    } else if(!elemento.pass) { 
      res.status(400).json ({ 
      error: 'Bad data', 
      description: 'Se precisa al menos un campo <pass>' 
      }); 
    }else { 
      db.user.findOne({ email: elemento.email }, (err, usuario)=>{
        if(err) return next(err);
        if(!usuario ){
            res.status(400).json({});
        }else{
            PassService.comparaPassword(elemento.pass, usuario.pass)
            .then(val => {
                if(val){
                  const ctoken = TokenService.crearToken(usuario);
                    res.json({
                        result: 'OK',
                        user: elemento,
                        token: ctoken
                    });
                }else{
                  res.json({
                    result: 'NOT OK'
                });
                }  
            });
        }
    })
    } 
  }); 
  
  
  
  app.post('/api/user', (req, res, next) => { 
    const elemento = req.body; 
    console.log(elemento);
  
    if (!elemento.nombre) { 
      res.status(400).json ({ 
      error: 'Bad data', 
      description: 'Se precisa al menos un campo <nombre>' 
      }); 
    } else if(!elemento.email) { 
      res.status(400).json ({ 
      error: 'Bad data', 
      description: 'Se precisa al menos un campo <email>' 
      }); 
    }else if(!elemento.pass) { 
      res.status(400).json ({ 
      error: 'Bad data', 
      description: 'Se precisa al menos un campo <pass>' 
      }); 
    } else { 
      db.user.findOne({ email: elemento.email }, (err, usuario)=>{
        if(err) return next(err);
        if(usuario ){
            res.status(400).json({});
        }else{
            PassService.encriptaPassword(elemento.pass)
            .then(passEnc => {
               
               const usuario = {
                    email: elemento.email,
                    name: elemento.nombre,
                    pass: passEnc,
                    signUpDate: moment().unix(),
                    lastLogin: moment().unix()
                    
                };
                db.user.save(usuario, (err, coleccionGuardada) => { 
                    if(err) return next(err);
                    const ctoken = TokenService.crearToken(usuario);
                    res.json({
                        result: 'OK',
                        user: coleccionGuardada,
                        token: ctoken
                    });
                }); 
            });
        }
    })
    } 
  });

//_______________________________user______________________________________


app.get('/api/user', auth, (req, res, next) =>{
    db.collection('usuario').find((err, colecciones) => {
        if(err) return next(err);
        res.json(colecciones);
    });
}); 



app.get('/api/user/:id', auth, (req, res, next) =>{
    req.collection.findOne({_id: id(req.params.id)}, (err, elemento) => {
        if(err) return next(err);
        res.json(elemento);
    });
});

app.post('/api/user', auth, (req,res, next) => {
    const elemento= req.body;

    if(!elemento.nombre){
        res.status(400).json({
            error: 'Bad data',
            description: 'Se precisa de al menos un campo <nombre>'
        });
    }else{
        req.collection.save(elemento, (err, coleccionGuardada) => {
            if(err) return next(err);
            res.json(coleccionGuardada);
        });
    }
});

app.put('/api/user/:id', auth, (req,res, next) =>{
    let elementoID = req.params.id;
    let elementoNuevo = req.body;
    req.collection.update ({_id: id(elementoID)}, {$set: elementoNuevo}, {safe: true, multi: false}, (err, elementoModif) =>{
        if(err) return next(err);
        res.json(elementoModif);
    });
});

app.delete('/api/user/:id', auth, (req, res, next)=>{
    let elementoID = req.params.id;

    req.collection.remove({_id: id(elementoID)}, (err, resultado) =>{
        if(err) return next(err);
        res.json(resultado);
    });
});

https.createServer( OPTIONS_HTTPS, app).listen(port, () => { 
    console.log(`SEC WS API REST CRUD ejecutándose en https://localhost:${port}/api/products`);
});