'use sctrict'

const PassService = require('./services/pass.service');
const TokenService = require('./services/token.service');
const moment= require('moment');

//formato del hash
//      $2b$10$71l7ofCZmLunVf3.bUV9T.tUJQyJ3f2bXdxjl1iW3QYrJpbsJQrzm
//      ****--- *********************+++++++++++++++++++++++++++++++
//      Alg Cost        Salt                    Hash
const bcrypt = require('bcrypt');


const token = TokenService.creaToken(usuario);
console.log(usuario);

TokenService.decodificaToken(token).then(userID =>{
    console.log(`ID1: ${userID}`);
}).catch(err => console.log(err));


PassService.encriptaPassword(usuario.password).then( hash =>{
    usuario.password=hash;
    console.log(usuario);
    
    
});

PassService.comparaPassword( miPass, usuario.password).then( isOk =>{
    if(isOk){
        console.log('p1: passwod correcto');
    }else{
        console.log('p1: passwod incorrecto');
    }
}).catch( err => console.log(err));


//salt=bcrypt.salt( 10 );
//hash = bcrypt.hash(miPass, salt);
//db.users.update(id, hash);
//db.account.hash.update(id, salt);

//creación del salt
/*
bcrypt.genSalt(15, (err, salt) =>{
    console.log(`Salt 1: ${salt}`);

    bcrypt.hash( miPass, salt, (err, hash) =>{
        if(err) console.log(err);
        else console.log(`hash 1: ${hash}`);
    });
});
*/
//crear hash directamente
/*
bcrypt.hash( miPass, 10, (err, hash) =>{
    if(err) console.log(err);
    else {
        console.log(`Hash 2: ${hash}`);

        // Comprobacion del hash
        bcrypt.compare( miPass, hash, (err, res) =>{
            if(err) console.log(err);
            else {
                console.log(`Result 2.1: ${res}`);
            }
        });

        bcrypt.compare( badPass, hash, (err, res) =>{
            if(err) console.log(err);
            else {
                console.log(`Result 2.2: ${res}`);
            }
        });
    }
});
*/