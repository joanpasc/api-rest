'use strict'

const bcrypt= require('bcrypt');

//formato del hash
//      $2b$10$71l7ofCZmLunVf3.bUV9T.tUJQyJ3f2bXdxjl1iW3QYrJpbsJQrzm
//      ****--- *********************+++++++++++++++++++++++++++++++
//      Alg Cost        Salt                    Hash

function encriptaPassword( password){
    return bcrypt.hash(password, 10);
}

function comparaPassword(password, hash){
    return bcrypt.compare(password, hash);
}

module.exports = {
    encriptaPassword,
    comparaPassword
};